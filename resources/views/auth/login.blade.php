@extends('layouts.backend')

@section('content')

<div id="bg-overlay" style='background-image: url("http://localhost/laraveldemo/public/backend/img/loginback.jpg");' class="bg-img"></div>
<div class="cls-content">
  <div class="cls-content-sm panel">
      <div class="panel-body">
          <div class="mar-ver pad-btm">
              <h1 class="h3">Account Login</h1>
              <p>Sign In to your account</p>
          </div>
          <form method="POST" action="{{ route('login') }}">
            @csrf
              <div class="form-group">
                  <input type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Enter Email..." autofocus>
                  @error('email')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                  @enderror
              </div>
              <div class="form-group">
                  <input type="password" class="form-control  @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">
                  @error('password')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
              </div>
              <div class="checkbox pad-btm text-left">
                  <input name="remember" id="demo-form-checkbox remember" {{ old('remember') ? 'checked' : '' }} class="magic-checkbox" type="checkbox">
                  <label for="demo-form-checkbox">{{ __('Remember Me') }}</label>
              </div>
              <button class="btn btn-primary btn-lg btn-block" type="submit">Sign In</button>
          </form>
      </div>
      <div class="pad-all">
        <a href="{{ route('password.request') }}" class="btn-link mar-rgt">{{ __('Forgot Your Password?') }}</a>
    </div>
  </div>
</div>
@endsection