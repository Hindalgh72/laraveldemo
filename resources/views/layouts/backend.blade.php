<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
 
  <link href='../../../fonts.googleapis.com/css13b7.css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
    <!--Bootstrap Stylesheet [ REQUIRED ]-->
    <link href="{{asset('backend/css/bootstrap.min.css')}}" rel="stylesheet">
    <!--Nifty Stylesheet [ REQUIRED ]-->
    <link href="{{asset('backend/css/nifty.min.css')}}" rel="stylesheet">
    <!--Nifty Premium Icon [ DEMONSTRATION ]-->
    <link href="{{asset('backend/css/demo/nifty-demo-icons.min.css')}}" rel="stylesheet">
    <!--Pace - Page Load Progress Par [OPTIONAL]-->
    <link href="{{asset('backend/plugins/pace/pace.min.css')}}" rel="stylesheet">
    <script src="{{asset('backend/plugins/pace/pace.min.js')}}"></script>
    <!--Ion Icons [ OPTIONAL ]-->
    <link href="{{asset('backend/plugins/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
    <link href="{{asset('backend/css/developer.css')}}" rel="stylesheet">
</head>
<body>
  <div id="container" class="cls-container">
    @yield('content')
  </div>
<!-- jQuery -->
<script src="{{asset('backend/js/jquery.min.js')}}"></script>
<!--BootstrapJS [ RECOMMENDED ]-->
<script src="{{asset('backend/js/bootstrap.min.js')}}"></script>
<!--NiftyJS [ RECOMMENDED ]-->
<script src="{{asset('backend/js/nifty.min.js')}}"></script>
<!--Flot Chart [ OPTIONAL ]-->
<script src="{{asset('backend/plugins/flot-charts/jquery.flot.min.js')}}"></script>
<script src="{{asset('backend/plugins/flot-charts/jquery.flot.resize.min.js')}}"></script>
<script src="{{asset('backend/plugins/flot-charts/jquery.flot.tooltip.min.js')}}"></script>
<!--Sparkline [ OPTIONAL ]-->
<script src="{{asset('backend/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('backend/js/developer.js')}}"></script>

</body>
</html>
