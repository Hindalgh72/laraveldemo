{{--
	DOCUMENTATION
	-------------------------------------------------------------------------------------------------

	1. Include Scripts into form:
		If you want to include scripts into form, 
		Enter below script into your form array element.

		You may pass either script file location.
		'include_scripts' => '<script src="'. asset('your-script-location.js'). '"></script>',

		Or, your custom script.
		'include_scripts' => '<script type="text/javascript">
			$(document).ready(function () {
				your script
			});
		</script>',	

	2. Custom Buttons:
		If you want to show custom buttons instead of
		this form's inbuild button, Enter below script
		into your form array element.

		'custom_buttons' => [
            [
                'type' 			=> 'submit',
                'text' 			=> '<i class="material-icons">save</i> <span>Save Changes</span>',
                'attributes' 	=> [
                    'class' 	=> 'btn bg-indigo btn-lg waves-effect',
                    'id'    	=> 'submit-button-id'
                ]
            ],
            [
            	'type' 			=> 'button',
            	'text' 			=> '<i class="material-icons">arrow_back</i> <span>Back</span>',
            	'attributes' 	=> [
                    'class' 	=> 'btn bg-info btn-lg waves-effect',
                    'id'    	=> 'back-button-id',
                    'onclick' 	=> 'your custom script'
                ]
            ]
        ],
--}}
@extends('backend.layouts.default', ['title' => $module])
@section('content')
<div class="panel-body">
    @if($id)
    {!! Form::model($data, [
    'method' => 'PATCH',
    'route' => [$form['route'], $id],
    'class' => 'form-horizontal',
    'enctype'=>'multipart/form-data'
    ]) !!}
    @else
    {!! Form::open(array('route' => $form['route'],'method'=>'POST', 'enctype'=>'multipart/form-data')) !!}
    @endif
    @if(!empty($form['fields']))
    @php ($editorScript = 0)
    @foreach($form['fields'] as $key => $value)
    @php ($attributes = (isset($value['attributes']) ? $value['attributes'] : []))
    @php ($attributes['id'] = (isset($attributes['id']) ? $attributes['id'] : $key))
    @php ($attributes['class'] = (isset($attributes['class']) ? $attributes['class'] : 'form-control'))
    @php ($inputValue = (isset($value['value']) ? $value['value'] : null))
    @if(!in_array($value['type'], ['html', 'include', 'hidden']))
    @php ($attributes['class'] .= ($errors->has($key) ? ' is-invalid' : ''))
    <div class="{{ isset($value['width']) ? $value['width'] : 'col-lg-6 col-md-6 col-xs-12 col-sm-12' }}"
        id="row-{{ $key }}">
        <div class="form-group{{ !in_array($value['type'], ['select', 'editor']) ? ' form-float' : '' }}{{$errors->has($key) ? ' has-error' : ''}}">
                @if(isset($value['label']) && $value['label'])
                <label for="{{ $attributes['id'] }}" class="form-label">{!! $value['label'] .
                    (isset($attributes['required']) ? ' <span class="text-danger">*</span>' : '') !!}:</label>
                @endif
                @if($value['type'] == 'text')
                {!! Form::text($key, $inputValue, $attributes) !!}
                @elseif($value['type'] == 'label')
                @php ($attributes['readonly'] = 'true')
                @php ($attributes['disabled'] = 'true')
                {!! Form::text($key, $inputValue, $attributes) !!}
                @elseif($value['type'] == 'email')
                {!! Form::email($key, $inputValue, $attributes) !!}
                @elseif($value['type'] == 'password')
                {!! Form::password($key, $attributes) !!}
                @elseif($value['type'] == 'textarea')
                {!! Form::textarea($key, $inputValue, $attributes) !!}
                @elseif($value['type'] == 'editor')
                @php ($attributes['class'] .= ' ckeditor')
                {!! Form::textarea($key, $inputValue, $attributes) !!}
                @if($editorScript == 0)
                @push('page_script')
                <script>
                    CKEDITOR.replace('.ckeditor');
                </script>
                @endpush
                @php ($editorScript = 1)
                @endif
                @elseif($value['type'] == 'select')
                {!! Form::select($key, $value['options'], $value['value'], $attributes) !!}
                @elseif($value['type'] == 'radio')
                <div class="clearfix"></div>
                <div class="row">
                    @foreach($value['options'] as $k => $option)
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="icheck-primary d-inline">
                            <input name="{{ $key }}" type="radio" value="{{ $k }}" id="{{ $key . '-' . $k }}"
                                {{ $value['value'] == $k ? 'checked' : '' }} />
                            <label for="{{ $key . '-' . $k }}">{{ $option }}</label>
                        </div>
                    </div>
                    @endforeach
                </div>
                @elseif($value['type'] == 'checkbox')
                <div class="clearfix"></div>
                <div class="row">
                    @foreach($value['options'] as $k => $option)
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="icheck-primary d-inline">
                            <input type="checkbox" name="{{ $key }}[]" value="{{ $k }}" id="{{ $key . '-' . $k }}"
                                {{ (is_array($value['value']) && in_array($k, $value['value'])) ? 'checked' : '' }} />
                            <label for="{{ $key . '-' . $k }}">{{ $option }}</label>
                        </div>
                    </div>
                    @endforeach
                </div>
                @elseif($value['type'] == 'file')
                {!! Form::file($key) !!}
                @if($value['value'])
                @if(isset($value['attributes']))
                @if(in_array($value['attributes']['file_mime'], ['image/jpeg', 'image/png', 'image/gif']))
                <img src="{{ $value['value'] }}" class="img-custom-laravel" />
                @endif
                @else
                <a href="{{ $value['value'] }}" target="_blank">View File</a>
                @endif
                @endif
                @endif
            @if(isset($value['help']))
            <span class="form-text text-muted">{{ $value['help'] }}</span>
            @endif
            @if ($errors->has($key))
            <small style="" class="help-block" data-bv-for="{{$key}}">{{ $errors->first($key) }}</small>
            @endif
        </div>
    </div>
    @elseif($value['type'] == 'hidden')
    {!! Form::hidden($key, $inputValue) !!}
    @elseif($value['type'] == 'html')
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">{!! $value['value'] !!}</div>
    <div class="clearfix"></div>
    @elseif($value['type'] == 'include')
    @include($value['value'])
    @endif
    @endforeach
    @endif
    @if(isset($include_page))
    @include($include_page)
    @endif
    <div class="clearfix"></div>
    @if(!isset($form['custom_buttons']))
    <button type="submit" class="btn btn-primary">{!! (isset($form['submit_text']) ? $form['submit_text']
        : 'Save Changes') !!}</button>
    @if(isset($form['back_route']))
    <a href="{{ $form['back_route'] }}" class="btn btn-mint">Back</a>
    @endif
    @else
    @foreach($form['custom_buttons'] as $button)
    <button type="{{ isset($button['type']) ? $button['type'] : 'submit' }}" {!! isset($button['attributes']) ?
        \App\Helpers\Helper::getAttr($button['attributes']) : '' !!}>{!! $button['text'] !!}</button>
    @endforeach
    @endif
    {!! Form::close() !!}
    <div class="clearfix"></div>
</div>

@endsection

@if(isset($form['include_scripts']))
@push('page_script')
{!! $form['include_scripts'] !!}
@endpush
@endif