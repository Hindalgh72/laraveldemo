@extends('backend.layouts.default', ['page_heading' => 'Dashboard', 'no_card_view' => true])
@section('content')
<!-- Main content -->
<!-- Default box -->
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">Inline Form</h3>
    </div>
    <div class="panel-body">
        Start creating your amazing application!
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        Footer
    </div>
    <!-- /.card-footer-->
</div>
<!-- /.card -->
@endsection