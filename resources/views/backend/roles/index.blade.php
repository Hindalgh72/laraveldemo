@php ($headerOption = [ 'title' => $module, 'header_button' =>
$permission['create'] ? '<div class="btn-group"><a class="btn btn-primary"
        href="'. route($routePrefix . '.create') .'" title="Add New Record">Add New</a><a href="javascript:void(0)"
        class="btn btn-info filter-btn" title="Filter Record">Search</a></div>' : '<a
    href="javascript:void(0)" class="btn btn-info filter-btn" title="Filter Record">Search</a>' ])
@extends('backend.layouts.default',$headerOption)
@section('content')
<div class="panel-body">
    <div class="pad-btm form-inline filter-frm-wrap {{ request()->has('q') ? '' : 'd-none'}}">
        <div class="row">
            <div class="col-sm-6"></div>
            <div class="col-sm-6 table-toolbar-right">
                <form method="get">
                    <div class="form-group">
                        <input id="demo-input-search2" name="q" type="text" value="{{isset($searchParam) ? $searchParam:''}}" placeholder="Search" class="form-control" autocomplete="off">
                    </div>
                    <div class="btn-group">
                        <button type="submit" class="btn btn-mint">Filter</button>
                        <a href="{{route($routePrefix.'.index')}}" class="btn btn-purple">Reset</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <table id="example2" class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>Title</th>
                @if($permission['edit'] || $permission['destroy'])
                <th width="15%" style="text-align: right;">Action</th>
                @endif
            </tr>
        </thead>
        <tbody>
            @if(count($data)>0)
                @foreach ($data as $value)
                <tr>
                    <td>{{ $value->title }}</td>
                    @if($permission['edit'] || $permission['destroy'])
                    <td class="text-right">
                        @if($permission['manageRole'])
                            <a href="{{ route('permissions.manage_role',$value->id) }}" data-toggle="tooltip" class="btn btn-dark btn-sm btn-icon" data-toggle="tooltip" title="Manag Role Permission"><i class="ion-ios-body"></i></a>
                        @endif
                        @if($permission['edit'] && $value->is_private!=1 || Auth::user()->id=='1')
                        <a href="{{ route($routePrefix . '.edit',$value->id) }}" class="btn btn-mint btn-sm btn-icon" data-toggle="tooltip" title="Edit"><i class="ion-edit"></i></a>
                        @endif
                        @if($permission['destroy'] && $value->is_private!=1 || Auth::user()->id=='1')
                        <a class="btn btn-sm btn-danger btn-icon delete-btn" title="" data-form-id="delete-form-{{$value->id}}" title="Delete"><i class="ion-trash-a"></i></a>
                        {!! Form::open([
                        'method' => 'DELETE',
                        'route' => [
                        $routePrefix . '.destroy',
                        $value->id
                        ],
                        'id' => 'delete-form-' . $value->id
                        ]) !!}
                        {!! Form::close() !!}
                        @endif
                    </td>
                    @endif
                </tr>
                @endforeach
            @else
                <tr><td colspan="5"><div class="alert alert-danger text-center">No Data</div></td></tr>
            @endif
        </tbody>
    </table>
</div>
<div class="panel-footer text-right">
        {!! $data->render() !!}
</div>
@endsection