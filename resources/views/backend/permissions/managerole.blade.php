@extends('backend.layouts.default')
@section('content')
    <div class="panel-body">
        <form action="{{route($route,$id)}}" method="post">
            @csrf
            @method('patch')
            @foreach ($sorted_list as $key=>$item)
                <h5>@php $split=explode('Controller',$key); echo $split[0]; @endphp</h5>
                <div class="row">
                    @foreach ($item as $subitem)
                        <div class="col-sm-2">
                            <input id="{{'permission_id-'.$subitem['id']}}" name="permission_id[]" value="{{$subitem['id']}}" class="magic-checkbox" type="checkbox" {{(is_array($value) && in_array($subitem['id'],$value)) ? 'checked':''}}>
                            <label for="{{'permission_id-'.$subitem['id']}}">{{$subitem['p_type']}}</label>
                        </div>
                    @endforeach
                </div>
                <hr>
            @endforeach
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection