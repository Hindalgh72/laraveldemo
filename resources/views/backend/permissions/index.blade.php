@php ($headerOption = [ 'title' => $module, 'header_button' =>
$permission['create'] ? '<div class="btn-group"><a class="btn btn-primary"
        href="'. route($routePrefix . '.create') .'" title="Add New Record">Add New</a><a href="javascript:void(0)"
        class="btn btn-info filter-btn" title="Filter Record">Search</a></div>' : '<a
    href="javascript:void(0)" class="btn btn-info filter-btn" title="Filter Record">Search</a>' ])
@extends('backend.layouts.default',$headerOption)
@section('content')
<div class="panel-body">
    <div class="pad-btm form-inline filter-frm-wrap {{ request()->has('q') ? '' : 'd-none'}}">
        <div class="row">
            <div class="col-sm-6"></div>
            <div class="col-sm-6 table-toolbar-right">
                <form method="get">
                    <div class="form-group">
                        <input id="demo-input-search2" name="q" type="text" value="{{isset($searchParam) ? $searchParam:''}}" placeholder="Search" class="form-control" autocomplete="off">
                    </div>
                    <div class="btn-group">
                        <button type="submit" class="btn btn-mint">Filter</button>
                        <a href="{{route('permissions.index')}}" class="btn btn-purple">Reset</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <table id="example2" class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>Permission types</th>
                <th>Module</th>
                <th>Function</th>
                @if($permission['edit'] || $permission['destroy'])
                <th width="15%" style="text-align: right;">Action</th>
                @endif
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $value)
            <tr>
                <td>{{ $value->p_type }}</td>
                <td>{{ $value->class }}</td>
                <td>{{ $value->method }}</td>
                @if($permission['edit'] || $permission['destroy'])
                <td class="text-right">
                    @if($permission['edit'])
                    <a href="{{ route($routePrefix . '.edit',$value->id) }}" class="btn btn-mint btn-sm btn-icon" data-toggle="tooltip" title="Edit"><i class="ion-edit"></i></a>
                    @endif
                    @if($permission['destroy'])
                    <a class="btn btn-sm btn-danger btn-icon delete-btn" title="" data-form-id="delete-form-{{$value->id}}" title="Delete"><i class="ion-trash-a"></i></a>
                    {!! Form::open([
                    'method' => 'DELETE',
                    'route' => [
                    $routePrefix . '.destroy',
                    $value->id
                    ],
                    'id' => 'delete-form-' . $value->id
                    ]) !!}
                    {!! Form::close() !!}
                    @endif
                </td>
                @endif
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<div class="panel-footer text-right">
        {!! $data->render() !!}
</div>
@endsection
@section('js')
$('#example2').DataTable({
"paging": false,
"lengthChange": false,
"searching": false,
"ordering": true,
"info": true,
"autoWidth": false,
});
@endsection