@if ($message = Session::get('success'))
<script>
    $.niftyNoty({
        type: 'success',
        container: 'floating',
        html: '{{$message}}',
        closeBtn: $("#demo-close-btn").prop('checked'),
        floating: {
            position: 'top-right',
            animationIn: 'jellyIn',
            animationOut: 'bounceOut'
        },
        focus: true,
        timer: 2500
    });
</script>
@endif
@if ($message = Session::get('error'))
<script>
    $.niftyNoty({
        type: 'danger',
        container: 'floating',
        html: '{{$message}}',
        closeBtn: $("#demo-close-btn").prop('checked'),
        floating: {
            position: 'top-right',
            animationIn: 'jellyIn',
            animationOut: 'bounceOut'
        },
        focus: true,
        timer: 2500
    });
</script>
@endif
@if ($message = Session::get('warning'))
<script>
    $.niftyNoty({
        type: 'warning',
        container: 'floating',
        html: '{{$message}}',
        closeBtn: $("#demo-close-btn").prop('checked'),
        floating: {
            position: 'top-right',
            animationIn: 'jellyIn',
            animationOut: 'bounceOut'
        },
        focus: true,
        timer: 2500
    });
</script>
@endif
@if ($message = Session::get('info'))
<script>
    $.niftyNoty({
        type: 'info',
        container: 'floating',
        html: '{{$message}}',
        closeBtn: $("#demo-close-btn").prop('checked'),
        floating: {
            position: 'top-right',
            animationIn: 'jellyIn',
            animationOut: 'bounceOut'
        },
        focus: true,
        timer: 2500
    });
</script>
@endif
@if ($errors->any())
<script>
    $.niftyNoty({
        type: 'danger',
        container: 'floating',
        html: 'Please check the below error',
        closeBtn: $("#demo-close-btn").prop('checked'),
        floating: {
            position: 'top-right',
            animationIn: 'jellyIn',
            animationOut: 'bounceOut'
        },
        focus: true,
        timer: 2500
    });
</script>
@endif

<script>
    function flashmessage(type,message){
        $.niftyNoty({
        type: type,
        container: 'floating',
        html: message,
        closeBtn: $("#demo-close-btn").prop('checked'),
        floating: {
            position: 'top-right',
            animationIn: 'jellyIn',
            animationOut: 'bounceOut'
        },
        focus: true,
        timer: 2500
    });
    }
</script>