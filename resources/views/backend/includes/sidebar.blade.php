@php $userRole = auth()->user()->roles->role_id; @endphp
@php $adminMenu = \App\AdminMenu::getMenu(); @endphp
@php $user=\App\User::with('files')->find(Auth::user()->id); $profile_file=isset($user['files']['full_url']) ? $user['files']['full_url']:asset('backend/img/defaultprofile.png')  @endphp
<!-- Main Sidebar Container -->
<nav id="mainnav-container">
    <div id="mainnav">
        <div id="mainnav-menu-wrap">
            <div class="nano">
                <div class="nano-content">
                    <div id="mainnav-profile" class="mainnav-profile">
                        <div class="profile-wrap text-center">
                            <div class="pad-btm">
                                <img class="img-circle img-md" src="{{ $profile_file }}" alt="Profile Picture">
                            </div>
                            <a href="#profile-nav" class="box-block" data-toggle="collapse" aria-expanded="false">
                                <span class="pull-right dropdown-toggle">
                                    <i class="dropdown-caret"></i>
                                </span>
                                <p class="mnp-name">{{Auth::user()->FullName}}</p>
                                <span class="mnp-desc">{{Auth::user()->email}}</span>
                            </a>
                        </div>
                        <div id="profile-nav" class="collapse list-group bg-trans">
                            <a href="{{route('users.profile')}}" class="list-group-item">
                                <i class="demo-pli-male icon-lg icon-fw"></i> View Profile
                            </a>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="list-group-item">
                                <i class="demo-pli-unlock icon-lg icon-fw"></i> Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </div>
                    <div id="mainnav-shortcut" class="hidden">
                        <ul class="list-unstyled shortcut-wrap">
                            <li class="col-xs-3" data-content="My Profile">
                                <a class="shortcut-grid" href="#">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-mint">
                                        <i class="demo-pli-male"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="col-xs-3" data-content="Messages">
                                <a class="shortcut-grid" href="#">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-warning">
                                        <i class="demo-pli-speech-bubble-3"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="col-xs-3" data-content="Activity">
                                <a class="shortcut-grid" href="#">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-success">
                                        <i class="demo-pli-thunder"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="col-xs-3" data-content="Lock Screen">
                                <a class="shortcut-grid" href="#">
                                    <div class="icon-wrap icon-wrap-sm icon-circle bg-purple">
                                        <i class="demo-pli-lock-2"></i>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <ul id="mainnav-menu" class="list-group">
                        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                        <li>
                            <a href="{{route('dashboard')}}">
                                <i class="demo-pli-home"></i>
                                <span class="menu-title">
                                    Dashboard
                                </span>
                            </a>
                        </li>
                        @foreach($adminMenu as $key => $menu)
                        @if($menu['child'])
                        <li class="{{(App\Helpers\Helper::getController()==$menu['class']) ? 'active-sub':''}}">
                            <a href="#">
                                <i class="{{ $menu['icon'] }}"></i>
                                <span class="menu-title">{{ $menu['menu'] }}</span>
                                <i class="arrow"></i>
                            </a>

                            <!--Submenu-->
                            <ul class="collapse">
                                @foreach($menu['child'] as $k => $childMenu)
                                <li class="{{(App\Helpers\Helper::getController()==$childMenu['class']) ? 'active-link':''}}">
                                    <a href="{{route($childMenu['url'], $childMenu['query_string'])}}">{{ $childMenu['menu'] }}</a>
                                </li>
                                @endforeach
                            </ul>
                        </li>
                        @else
                        <li class="{{(App\Helpers\Helper::getController()==$menu['class']) ? 'active-link':''}}">
                            <a href="{{route($menu['url'], $menu['query_string'])}}">
                                <i class="{{ $menu['icon'] }}"></i>
                                <span class="menu-title">
                                    {{ $menu['menu'] }}
                                </span>
                            </a>
                        </li>
                        @endif
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>