@if(isset($breadcrumb))

<!--Breadcrumb-->
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="demo-pli-home"></i></a></li>
    @php ($breadcrumbLimit = count($breadcrumb))
    @php ($currentBreadcrumb = 1)
    @foreach($breadcrumb as $key => $val)
    <li>
        @if($currentBreadcrumb != $breadcrumbLimit)
        <a href="{{ $key }}">{{ $val }}</a>
        @else
        {{ $val }}
        @endif
        @php ($currentBreadcrumb++)
    </li>
    @endforeach
</ol>
<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
<!--End breadcrumb-->
@endif