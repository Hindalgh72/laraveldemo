<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <base href="/latestlaravel/public/">

    <title>{{ isset($page_heading) ? $page_heading : config('app.name', 'Laravel') }}</title>

    <link href='../../../fonts.googleapis.com/css13b7.css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
    <!--Bootstrap Stylesheet [ REQUIRED ]-->
    <link href="{{asset('backend/css/bootstrap.min.css')}}" rel="stylesheet">
    <!--Nifty Stylesheet [ REQUIRED ]-->
    <link href="{{asset('backend/css/nifty.min.css')}}" rel="stylesheet">
    <!--Nifty Premium Icon [ DEMONSTRATION ]-->
    <link href="{{asset('backend/css/demo/nifty-demo-icons.min.css')}}" rel="stylesheet">
    <!--Pace - Page Load Progress Par [OPTIONAL]-->
    <link href="{{asset('backend/plugins/pace/pace.min.css')}}" rel="stylesheet">
    <script src="{{asset('backend/plugins/pace/pace.min.js')}}"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@8.19.0/dist/sweetalert2.min.css">
    <!--Ion Icons [ OPTIONAL ]-->
    <link href="{{asset('backend/plugins/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
    <link href="{{asset('backend/css/developer.css')}}" rel="stylesheet">
    <!--Bootstrap Select [ OPTIONAL ]-->
    <link href="{{asset('backend/plugins/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet">
    <!--Switchery [ OPTIONAL ]-->
    <link href="{{asset('backend/plugins/switchery/switchery.min.css')}}" rel="stylesheet">

    <!--Select2 [ OPTIONAL ]-->
    <link href="{{asset('backend/plugins/select2/css/select2.min.css')}}" rel="stylesheet">
    @yield('topcss')
</head>

<body>
    <div id="container" class="effect aside-float aside-bright mainnav-lg">
        <!-- Navbar -->
        @include('backend.includes.navbar')
        <!-- /.navbar -->

        @include('backend.includes.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="boxed">
            <!-- Content Header (Page header) -->
            <div id="content-container">
                <div id="page-head">
                    @if(isset($page_heading))
                    <div id="page-title">
                        <h1 class="page-header text-overflow">{{ $page_heading }}</h1>
                    </div>
                    @endif
                    @include('backend.includes.breadcrumb')
                </div><!-- /.container-fluid -->

                <div id="page-content">
                    @if(isset($extra_card))
                        @yield('extracontent')
                    @endif
                    @if(!isset($no_card_view))
                    <div class="panel">
                        @if(isset($title) || isset($header_button))
                            <div class="panel-heading">
                                @if(isset($header_button))
                                    <div class="panel-control">
                                        {!! $header_button !!}
                                    </div>
                                @endif
                                @if(isset($title))
                                    <h3 class="panel-title">{{ $title }}</h3>
                                @endif
                            </div>
                        @endif
                        @yield('content')
                    </div>
                    @else
                    @yield('content')
                    @endif
                </div>
            </div>
        </div>
        <!-- /.content-wrapper -->
        @include('backend.includes.footer')
        <button class="scroll-top btn">
            <i class="pci-chevron chevron-up"></i>
        </button>
    </div>
    <!-- ./wrapper -->
    @yield('modalsection')
    <!-- REQUIRED SCRIPTS -->
    <!-- jQuery -->
    <script src="{{asset('backend/js/jquery.min.js')}}"></script>
    <!--BootstrapJS [ RECOMMENDED ]-->
    <script src="{{asset('backend/js/bootstrap.min.js')}}"></script>
    <!--NiftyJS [ RECOMMENDED ]-->
    <script src="{{asset('backend/js/nifty.min.js')}}"></script>
    <!--Bootstrap Select [ OPTIONAL ]-->
    <script src="{{asset('backend/plugins/bootstrap-select/bootstrap-select.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.19.0/dist/sweetalert2.min.js"></script>
    <!--Select2 [ OPTIONAL ]-->
    <script src="{{asset('backend/plugins/select2/js/select2.min.js')}}"></script>
    <!--Switchery [ OPTIONAL ]-->
    <script src="{{asset('backend/plugins/switchery/switchery.min.js')}}"></script>
    <script src="{{asset('backend/js/developer.js')}}"></script>
    @include('backend.includes.toast-flash')
    @stack('page_script')
    @yield('bottomjs')
</body>

</html>