<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('admin')->group(function () {
    Route::get('/', function () {
        return redirect('admin/dashboard');
    });
    Auth::routes(['register' => false]);
});

Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function () {

    Route::get('dashboard', 'Backend\DashboardController@index')->name('dashboard');

    Route::group(['middleware' => 'permissionChecking'], function () {
        Route::get('users/status/{id}', 'Backend\UserController@togglestatus')->name('users.status');
        Route::get('permissions/manage_role/{id}', 'Backend\PermissionController@managerole')->name('permissions.manage_role');
        Route::patch('permissions/assign/{id}', "Backend\PermissionController@assignpermission")->name('permissions.assign');
        Route::group(['prefix' => 'menus'], function () {
            Route::get('index/{parent_id}', 'Backend\MenuController@index')->name('menus.index');
            Route::get('create/{parent_id}', 'Backend\MenuController@create')->name('menus.create');
            Route::get('edit/{parent_id}/{id}', 'Backend\MenuController@edit')->name('menus.edit');
            Route::post('store', 'Backend\MenuController@store')->name('menus.store');
            Route::patch('update/{id}', 'Backend\MenuController@update')->name('menus.update');
            Route::delete('destroy/{parent_id}/{id}', 'Backend\MenuController@destroy')->name('menus.destroy');
        });
        Route::get('user/profile','Backend\UserController@editProfile')->name('users.profile');
        Route::patch('user/profile/{id}','Backend\UserController@saveProfile')->name('users.saveProfile');

        Route::resources([
            'roles' => 'Backend\RoleController',
            'users' => 'Backend\UserController',
            'permissions' => 'Backend\PermissionController',
        ]);



        


    });
});



Route::get('/home', 'HomeController@index')->name('home');
