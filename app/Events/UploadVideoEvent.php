<?php

namespace App\Events;

// use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class UploadVideoEvent 
{
    use SerializesModels;

    public $lesson_id;
    public $unique_key;
    public $fileName;
    public $file_id; 
    public $file_path;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($lesson_id, $unique_key, $fileName, $file_id,$file_path)
    {
        $this->lesson_id = $lesson_id;
        $this->unique_key   = $unique_key;
        $this->fileName     = $fileName;
        $this->file_id = $file_id;
        $this->file_path = $file_path;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
