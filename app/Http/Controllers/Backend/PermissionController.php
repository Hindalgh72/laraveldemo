<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Helper\Helper;
use App\Permission;
use App\Http\Controllers\Backend\BaseController;
use Illuminate\Support\Facades\Auth;

class PermissionController extends BaseController
{

    private $_module;
    private $_offset;
    private $_routePrefix;

    public function __construct()
    {
        $this->_module      = 'Manage Permission';
        $this->_offset      = 10;
        $this->_routePrefix = 'permissions';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $page = '')
    {
        $page=$page ? $page : $this->_offset;
        $permission = \App\Permission::checkModulePermissions();
        $input=$request->all();
        $searchParam = isset($input['q']) ? $input['q'] : '';

        $permissionq = Permission::query();
        if($searchParam){
            $permissionq->where('p_type','LIKE','%'.$searchParam.'%');
            $permissionq->orWhere('class','LIKE','%'.$searchParam.'%');
            $permissionq->orWhere('method','LIKE','%'.$searchParam.'%');
        }
        $permissionq->orderBy('class', 'ASC');
        $permissionq->orderBy('id', 'ASC');
        $data=$permissionq->paginate($page);
        $breadcrumb = [
            route($this->_routePrefix . '.index') => $this->_module,
        ];
        $module = $this->_module;
        $page_heading="Permission List";
        $routePrefix = $this->_routePrefix;
        return view('backend.' . $this->_routePrefix . '.index', compact(
            'data',
            'breadcrumb',
            'module',
            'permission',
            'routePrefix',
            'page_heading',
            'searchParam'
        ))
            ->with('i', ($request->input('page', 1) - 1) * $this->_offset);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->__formUiGeneration();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->__formPost($request);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->__formUiGeneration($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->__formPost($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Permission::find($id);

        $return = \App\Helpers\Helper::notValidData($data, $this->_routePrefix . '.index');
        if ($return) {
            return $return;
        }

        $data->delete();

        return redirect()->route($this->_routePrefix . '.index')
            ->with('success', 'Site Content deleted successfully');
    }

    /**
     * ui parameters for form add and edit
     *
     * @param  string $id [description]
     * @return [type]     [description]
     */
    private function __formUiGeneration($id = '')
    {
        $data       = [];
        $moduleName = 'Add Permission';

        if ($id) {
            $data       = Permission::find($id);

            $return = \App\Helpers\Helper::notValidData($data, $this->_routePrefix . '.index');
            if ($return) {
                return $return;
            }

            $moduleName = 'Edit Permission';
        } else {
            $data = new Permission;
        }

        $module     = $this->_module . ' | ' . $moduleName;
        $breadcrumb = [
            route($this->_routePrefix . '.index') => $this->_module,
            '#'                                   => $moduleName,
        ];

        $form = [
            'route'      => $this->_routePrefix . ($id ? '.update' : '.store'),
            'back_route' => route($this->_routePrefix . '.index'),
            'fields'     => [
                'p_type'             => [
                    'type'       => 'text',
                    'label'      => 'Permission Type',
                    'help'       => 'Maximum 255 characters',
                    'attributes' => ['required' => true],
                ],
                'class'             => [
                    'type'       => 'text',
                    'label'      => 'Controller Name',
                    'help'       => 'Maximum 50 characters',
                    'attributes' => ['required' => true],
                ],
                'method'             => [
                    'type'       => 'text',
                    'label'      => 'Controller Method Name',
                    'help'       => 'Maximum 50 characters',
                    'attributes' => ['required' => true],
                ],
            ],
        ];

        return view('backend.components.admin-form', compact('data', 'id', 'form', 'breadcrumb', 'module'));
    }

    /**
     * Form post action
     *
     * @param  Request $request [description]
     * @param  string  $id      [description]
     * @return [type]           [description]
     */
    private function __formPost(Request $request, $id = '')
    {
        $this->validate($request, [
            'p_type'            => 'required|max:255',
            'class'             => 'required|max:50',
            'method'            => 'required|max:50',
        ]);

        $input = $request->all();


        if ($id) {
            $data       = Permission::find($id);

            $return = \App\Helpers\Helper::notValidData($data, $this->_routePrefix . '.index');
            if ($return) {
                return $return;
            }

            $data->update($input);
        } else {
            $data   = Permission::create($input);
        }

        return redirect()
            ->route($this->_routePrefix . '.index')
            ->with('success', 'Record has been successfully saved.');
    }

    public function manageRole(Request $request, $role_id = '')
    {
        if (!$role_id)
            return FALSE;
        $data       = [];

        $module_name = "Roles";
        $role = \App\Role::find($role_id);
        $currentUserRole = Auth::user()->roles->role_id;
        $module = "Manage Role Permisssion - " . $role->title;

        $list = Permission::select("permissions.*");
        if ($currentUserRole != 1) {
            $list->join('permission_roles AS pr', function ($join) use ($currentUserRole) {
                $join->on('pr.permission_id', '=', 'permissions.id')
                    ->where('pr.role_id', $currentUserRole);
            });
        }
        $list = $list->orderby('permissions.class', 'asc')
            ->orderby('permissions.p_type', 'asc')
            ->get();

        $sorted_list=array();
        foreach($list as $element){
            $sorted_list[$element['class']][]=$element;
        }

        $moduleName = 'Assign Permission';
        $breadcrumb = [
            route($this->_routePrefix . '.index') => $this->_module,
            '#'                                   => $moduleName,
        ];
        $value = \App\PermissionRole::where("role_id", $role_id)
            ->pluck('permission_id')
            ->all();
        $route=$this->_routePrefix.'.assign';

        $id = $role_id;
        return view('backend.permissions.managerole', compact('data', 'id', 'route', 'value','breadcrumb', 'module','sorted_list'));
    }

    /**
     * Form post action
     *
     * @param  Request $request [description]
     * @param  string  $id      [description]
     * @return [type]           [description]
     */
    public function assignPermission(Request $request, $id = '')
    {
        $input = $request->all();
        $rolePermission = [];
        \App\PermissionRole::where('role_id', $id)->delete();

        if (!empty($input['permission_id'])) {
            foreach ($input['permission_id'] as $key => $value) {
                $rolePermission[] = [
                    'permission_id' => $value,
                    'role_id' => $id
                ];
            }

            \App\PermissionRole::insert($rolePermission);
        }


        return redirect()
            ->route('roles.index')
            ->with('success', 'Record has been successfully saved.');
    }
}
