<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\BaseController;

use Illuminate\Http\Request;
use App\AdminMenu;
use App\Helpers\Helper;

class MenuController extends BaseController
{

    private $_module;
    private $_offset;
    private $_routePrefix;

    public function __construct()
    {
        $this->_module      = 'Manage Admin Menu';
        $this->_offset      = 20;
        $this->_routePrefix = 'menus';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $parent_id = 0)
    {
        $permission = \App\Permission::checkModulePermissions();

        $data = AdminMenu::where("parent_id", $parent_id)
            ->paginate($this->_offset);

        $adminMenu = new AdminMenu();
        $adminMenu = $adminMenu->getParentMenu($parent_id);

        $breadcrumb[route($this->_routePrefix . '.index', 0)] = $this->_module;
        foreach ($adminMenu as $key => $value) {
            $breadcrumb[route($this->_routePrefix . '.index', $value['id'])] = $value['menu'];
        }

        // dd($permission);
        $module = $this->_module;
        $routePrefix = $this->_routePrefix;
        return view('backend.' . $this->_routePrefix . '.index', compact(
            'data',
            'breadcrumb',
            'module',
            'permission',
            'routePrefix',
            'parent_id'
        ))
            ->with('i', ($request->input('page', 1) - 1) * $this->_offset);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($parent_id = 0)
    {
        return $this->__formUiGeneration($parent_id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->__formPost($request);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($parent_id = 0, $id)
    {
        return $this->__formUiGeneration($parent_id, $id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->__formPost($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($parent_id = 0, $id)
    {
        $data = AdminMenu::find($id);

        $return = \App\Helpers\Helper::notValidData($data, $this->_routePrefix . '.index');
        if ($return) {
            return $return;
        }

        $data->delete();

        return redirect()->route($this->_routePrefix . '.index',$parent_id)
            ->with('success', 'Site Content deleted successfully');
    }

    /**
     * ui parameters for form add and edit
     *
     * @param  string $id [description]
     * @return [type]     [description]
     */
    private function __formUiGeneration($parent_id = 0, $id = '')
    {
        $data       = [];
        $moduleName = 'Add Admin Menu';

        if ($id) {
            $data       = AdminMenu::find($id);

            $return = \App\Helpers\Helper::notValidData($data, $this->_routePrefix . '.index');
            if ($return) {
                return $return;
            }

            $moduleName = 'Edit Admin Menu';
        } else {
            $data = new AdminMenu;
        }


        $adminMenu = new AdminMenu();
        $adminMenu = $adminMenu->getParentMenu($parent_id);

        $breadcrumb[route($this->_routePrefix . '.index', 0)] = $this->_module;
        foreach ($adminMenu as $key => $value) {
            $breadcrumb[route($this->_routePrefix . '.index', $value['id'])] = $value['menu'];
        }

        $module     = $this->_module . ' | ' . $moduleName;
        $breadcrumb['#'] = $moduleName;

        $form = [
            'route'      => $this->_routePrefix . ($id ? '.update' : '.store'),
            'back_route' => route($this->_routePrefix . '.index', $parent_id),
            'fields'     => [
                'parent_id' => [
                    'type' => 'hidden',
                    'value' => $parent_id
                ],
                'menu'             => [
                    'type'       => 'text',
                    'label'      => 'Menu Name',
                    'help'       => 'Maximum 255 characters',
                    'attributes' => ['required' => true],
                ],
                'icon'             => [
                    'type'       => 'text',
                    'label'      => 'Menu Icon',
                    'help'       => 'Maximum 50 characters',
                ],
                'class'             => [
                    'type'       => 'text',
                    'label'      => 'Controller Name',
                    'help'       => 'Maximum 50 characters',
                    'attributes' => ['required' => true],
                ],
                'method'             => [
                    'type'       => 'text',
                    'label'      => 'Controller Method Name',
                    'help'       => 'Maximum 50 characters',
                    'attributes' => !$parent_id ? [] : ['required' => true],
                ],
                'url'             => [
                    'type'       => 'text',
                    'label'      => 'URL',
                    'help'       => 'Maximum 255 characters',
                    'value'      => $data->url ? $data->url : '#'
                ],
                'query_string'             => [
                    'type'       => 'text',
                    'label'      => 'Query String',
                    'help'       => 'If you want to pass extra param through url. Maximum 255 characters'
                ],
                'display_order' => [
                    'type' => 'text',
                    'label' => 'Display Order',
                    'value' => $data->display_order ? $data->display_order : 0
                ],
                'status' => [
                    'type' => 'radio',
                    'label' => 'Status',
                    'options' => [1 => 'Enabled', 0 => 'Disabled'],
                    'value' => $data->status
                ],
            ],
        ];

        if ($parent_id && !$id) {
            $roles = \App\Role::where('uid', '>', 0)->pluck('title', 'id')->all();
            $permissionMenu = [
                'permission_html' => [
                    'type' => 'html',
                    'value' => '<h3>Permission Info</h3>'
                ],
                'permission_list'             => [
                    'type'       => 'text',
                    'label'      => 'Permission for Listing',
                    'help'       => 'Please input Method\'s name only. Maximum 255 characters',
                    'value'      => 'index'
                ],
                'permission_add'             => [
                    'type'       => 'text',
                    'label'      => 'Permission for Add new Item',
                    'help'       => 'Please input Method\'s name only. Maximum 255 characters',
                    'value'      => 'create'
                ],
                'permission_edit'             => [
                    'type'       => 'text',
                    'label'      => 'Permission for Edit an Item',
                    'help'       => 'Please input Method\'s name only. Maximum 255 characters',
                    'value'      => 'edit'
                ],
                'permission_delete'             => [
                    'type'       => 'text',
                    'label'      => 'Permission for Delete a Item',
                    'help'       => 'Please input Method\'s name only. Maximum 255 characters',
                    'value'      => 'destroy'
                ],
                'role_html' => [
                    'type' => 'html',
                    'value' => '<h3>Role Info</h3>'
                ],
                'role_ids'             => [
                    'type'       => 'checkbox',
                    'label'      => 'Assign permission for Roles',
                    'options'    => $roles,
                    'value'      => ''
                ]
            ];

            $form['fields'] = array_merge($form['fields'], $permissionMenu);
        }

        return view('backend.components.admin-form', compact('data', 'id', 'form', 'breadcrumb', 'module'));
    }

    /**
     * Form post action
     *
     * @param  Request $request [description]
     * @param  string  $id      [description]
     * @return [type]           [description]
     */
    private function __formPost(Request $request, $id = '')
    {
        $this->validate($request, [
            'menu'              => 'required|max:255',
            'class'             => 'required|max:50',
            'method'            => 'required|max:50',
            'display_order'     => 'required|numeric'
        ]);

        $input = $request->all();


        if ($id) {
            $data       = AdminMenu::find($id);

            $return = Helper::notValidData($data, $this->_routePrefix . '.index');
            if ($return) {
                return $return;
            }

            $data->update($input);
        } else {
            $data   = AdminMenu::create($input);

            // if parent id exists
            // it will insert roles and
            // assign permission.
            if ($input['parent_id'] && isset($input['permission_add'])) {
                // $date = date("Y-m-d H:i:s");
                $permissionList = \App\Permission::create([
                    'p_type' => strtolower($input['menu']) . ' list',
                    'class'  => $input['class'],
                    'method' => $input['permission_list']
                ]);

                $permissionAdd = \App\Permission::create([
                    'p_type' => strtolower($input['menu']) . ' add',
                    'class'  => $input['class'],
                    'method' => $input['permission_add']
                ]);

                $permissionEdit = \App\Permission::create([
                    'p_type' => strtolower($input['menu']) . ' edit',
                    'class'  => $input['class'],
                    'method' => $input['permission_edit']
                ]);

                $permissionDelete = \App\Permission::create([
                    'p_type' => strtolower($input['menu']) . ' delete',
                    'class'  => $input['class'],
                    'method' => $input['permission_delete']
                ]);

                if (isset($input['role_ids'])) {
                    $rolePermission = [];
                    foreach ($input['role_ids'] as $role) {
                        $rolePermission[] = [
                            'permission_id' => $permissionList->id,
                            'role_id' => $role
                        ];

                        $rolePermission[] = [
                            'permission_id' => $permissionAdd->id,
                            'role_id' => $role
                        ];

                        $rolePermission[] = [
                            'permission_id' => $permissionEdit->id,
                            'role_id' => $role
                        ];

                        $rolePermission[] = [
                            'permission_id' => $permissionDelete->id,
                            'role_id' => $role
                        ];
                    }

                    \App\PermissionRole::insert($rolePermission);
                }
            }
        }

        return redirect()
            ->route($this->_routePrefix . '.index', $input['parent_id'])
            ->with('success', 'Record has been successfully saved.');
    }
}
