<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use View;
use File;
use Illuminate\Support\Facades\Storage;


class BaseController extends Controller
{
    public function __construct()
    {
        $actiona = app('request')->route()->getAction();
        $controllera = class_basename($actiona['controller']);

        list($controller, $action) = explode('@', $controllera);

        View::share('action', $action);
        View::share('controller', $controller);
    }
    
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }



    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function SaveFile($imageData)
    {
        $filename = time().rand(1000,5000). $imageData->getClientOriginalName();
        $originalFileName=$imageData->getClientOriginalName();
        $type=$imageData->getMimeType();
        Storage::disk('public')->putFileAs(
            'files/',
            $imageData,
            $filename
        );
        return array('name'=>$filename,'type'=>$type,'original_name'=>$originalFileName);
    }
}
