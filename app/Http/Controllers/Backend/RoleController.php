<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\BaseController;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoleController extends BaseController
{
    private $_module;
    private $_offset;
    private $_routePrefix;

    public function __construct()
    {
        $this->_module      = 'Manage Roles';
        $this->_offset      = 20;
        $this->_routePrefix = 'roles';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $permission = \App\Permission::checkModulePermissions();
        $manageRole = \App\Permission::checkModulePermissions(['manageRole'], 'PermissionController');
        $permission = array_merge($permission, $manageRole);

        $userId = Auth::user()->id;
        $query = Role::query();

        if($userId!='1'){
            $query->whereNotIn('id',['1','2']);
        }
        $data=$query->orderBy('id', 'DESC')->paginate($this->_offset);

        $breadcrumb = [
            route($this->_routePrefix . '.index') => $this->_module,
        ];
        $module = $this->_module;
        $routePrefix = $this->_routePrefix;
        return view('backend.' . $this->_routePrefix . '.index', compact(
            'data',
            'breadcrumb',
            'module',
            'permission',
            'routePrefix'
        ))
            ->with('i', ($request->input('page', 1) - 1) * $this->_offset);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->__formUiGeneration();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->__formPost($request);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->__formUiGeneration($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->__formPost($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Role::find($id);

        $return = \App\Helpers\Helper::notValidData($data, $this->_routePrefix . '.index');
        if ($return) {
            return $return;
        }
        if(count($data->users)>0){
            $data->users()->delete();
        }
        if(count($data->permissions)>0){
            $data->permissions()->delete();
        }
        $data->delete();

        return redirect()->route($this->_routePrefix . '.index')
            ->with('success', 'Role deleted successfully');
    }

    /**
     * ui parameters for form add and edit
     *
     * @param  string $id [description]
     * @return [type]     [description]
     */
    private function __formUiGeneration($id = '')
    {
        $data       = [];
        $moduleName = 'Add Role';

        if ($id) {
            $userId     = Auth::user()->id;
            $data       = Role::where('uid', '=', $userId)->find($id);

            $return = \App\Helpers\Helper::notValidData($data, $this->_routePrefix . '.index');
            if ($return) {
                return $return;
            }

            $moduleName = 'Edit Role';
        } else {
            $data = new Role;
        }

        $module     = $this->_module . ' | ' . $moduleName;
        $breadcrumb = [
            route($this->_routePrefix . '.index') => $this->_module,
            '#'                                   => $moduleName,
        ];

        $form = [
            'route'      => $this->_routePrefix . ($id ? '.update' : '.store'),
            'back_route' => route($this->_routePrefix . '.index'),
            'fields'     => [
                'title'             => [
                    'type'       => 'text',
                    'label'      => 'Title',
                    'help'       => 'Maximum 255 characters',
                    'attributes' => ['required' => true],
                ],
            ],
        ];

        return view('backend.components.admin-form', compact('data', 'id', 'form', 'breadcrumb', 'module'));
    }

    /**
     * Form post action
     *
     * @param  Request $request [description]
     * @param  string  $id      [description]
     * @return [type]           [description]
     */
    private function __formPost(Request $request, $id = '')
    {
        $this->validate($request, [
            'title'            => 'required|max:255',
        ]);

        $input = $request->all();
        $userId     = Auth::user()->id;

        if ($id) {
            $data       = Role::where('uid', '=', $userId)->find($id);

            $return = \App\Helpers\Helper::notValidData($data, $this->_routePrefix . '.index');
            if ($return) {
                return $return;
            }

            $data->update($input);
        } else {
            $input['uid'] = $userId;
            $input['status'] = '1';
            $data   = Role::create($input);
        }

        return redirect()
            ->route($this->_routePrefix . '.index')
            ->with('success', 'Record has been successfully saved.');
    }
}
