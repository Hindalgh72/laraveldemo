<?php

namespace App\Http\Controllers\Backend;

use App\Department;
use App\File;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Helpers\Helper;
use App\UserDepartment;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File as FacadesFile;
use Illuminate\Support\Facades\Session;

class UserController extends BaseController
{
    private $_module;
    private $_offset;
    private $_routePrefix;

    public function __construct()
    {
        $this->_module      = 'Manage Users';
        $this->_offset      = 10;
        $this->_routePrefix = 'users';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $page = '')
    {
        $page = $page   ?   $page   :   10;
        $permission = \App\Permission::checkModulePermissions(['index', 'create', 'edit', 'destroy']);
        $loggedInUserId=Auth::user()->id;
        $srch_params = $request->toArray();
        $srch_params['id_greater_than'] = $loggedInUserId;
        $user = User::query();
        $input = $request->all();
        $searchParam = isset($input['q']) ? $input['q'] : '';
        $sortName= isset($input['sort']) ? $input['sort']:'';
        $sortdirection= isset($input['direction']) ? $input['direction']:'';
        if ($searchParam) {
            $user->where(DB::raw("CONCAT(`first_name`, ' ', `last_name` ) LIKE '%" . $searchParam . "%'"));
            $user->orWhere("email", 'like', '%' . $searchParam . '%');
        }
        if($sortName && $sortdirection){
            if($sortName == 'name'){
                $user->orderByRaw('CONCAT(first_name, last_name) '.addslashes($sortdirection));
            }else{
                $user->orderBy($sortName,$sortdirection);
            }
        }
        if($loggedInUserId!='1'){
            $user->whereNotIn('id',['1','2',$loggedInUserId]);
        }
        $data = $user->paginate($page);

        $breadcrumb = [
            route($this->_routePrefix . '.index') => $this->_module,
        ];
        $module = $this->_module;
        $routePrefix = $this->_routePrefix;
        return view('backend.' . $this->_routePrefix . '.index', compact(
            'data',
            'breadcrumb',
            'module',
            'permission',
            'routePrefix',
            'searchParam'
        ))
            ->with('i', ($request->input('page', 1) - 1) * $this->_offset);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->__formUiGeneration();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->__formPost($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view($this->_routePrefix . '.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->__formUiGeneration($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->__formPost($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = User::find($id);

        $return = \App\Helpers\Helper::notValidData($data, $this->_routePrefix . '.index');
        if ($return) {
            return $return;
        }

        $data->delete();

        return redirect()
            ->route($this->_routePrefix . '.index')
            ->with('success', 'User deleted successfully');
    }

    /**
     * ui parameters for form add and edit
     *
     * @param  string $id [description]
     * @return [type]     [description]
     */
    private function __formUiGeneration($id = '')
    {
        $roles    = [];
        $userRole = [];

        $data       = [];
        $ownAccount = true;
        $moduleName = 'Add New User';
        $fileLocation = '';

        if ($id) {
            $data = User::with('files')->find($id);

            $return = \App\Helpers\Helper::notValidData($data, $this->_routePrefix . '.index');
            if ($return) {
                return $return;
            }

            $moduleName = 'Edit User';
        } else {
            $data = new User;
        }

        $module     = $this->_module . ' | ' . $moduleName;
        $breadcrumb = [
            route($this->_routePrefix . '.index') => $this->_module,
            '#'                                   => $moduleName,
        ];
        if (Auth::user()->id != $id) {
            $ownAccount = false;
            $userRole = Auth::user()->roles->role_id;
            $roles = \App\Role::where('id', '>=', $userRole)
                ->pluck('title', 'id')
                ->all();
        }
        $form = [
            'route'      => $this->_routePrefix . ($id ? '.update' : '.store'),
            'back_route' => route($this->_routePrefix . '.index'),
            'fields'     => [
                'first_name'             => [
                    'type'       => 'text',
                    'label'      => 'First Name',
                    'help'       => 'Maximum 100 characters',
                    'attributes' => ['required' => true],
                ],
                'middle_name'             => [
                    'type'       => 'text',
                    'label'      => 'Middle Name',
                    'help'       => 'Maximum 100 characters',
                ],
                'last_name'             => [
                    'type'       => 'text',
                    'label'      => 'Last Name',
                    'help'       => 'Maximum 100 characters',
                ],
                'email'            => [
                    'type'       => 'email',
                    'label'      => 'Email',
                    'help'       => 'Maximum 191 characters',
                    'attributes' => ['required' => true],
                ],
                'phone'            => [
                    'type'       => 'text',
                    'label'      => 'Phone Number',
                ],
                'password'         => [
                    'type'  => 'password',
                    'label' => 'Password',
                ],
                'confirm-password' => [
                    'type'  => 'password',
                    'label' => 'Confirm Password',
                ],
                'role_id' => [
                    'type' => 'select',
                    'label' => 'Role',
                    'options' => $roles,
                    'attributes' => ['width' => 'col-lg-4 col-md-4 col-sm-12 col-xs-12','class'=>'form-control selectpicker'],
                    'value' => isset($data->roles->role_id) ? $data->roles->role_id : 0
                ],
                'profile_image'=>[
                    'type'=>'file',
                    'value'=>isset($data['files']) ? $data['files']['full_url']:'',
                    'attributes'=>['file_mime'=>isset($data['files']) ? $data['files']['file_type']:''],
                ]
            ],
        ];

        if ($ownAccount) {
            unset($form['back_route']);
            unset($form['fields']['role_id']);
            unset($form['fields']['status']);
        }

        return view('backend.components.admin-form', compact('data', 'id', 'form', 'breadcrumb', 'module'));
    }

    /**
     * Form post action
     *
     * @param  Request $request [description]
     * @param  string  $id      [description]
     * @return [type]           [description]
     */
    private function __formPost(Request $request, $id = 0)
    {
        $validationRules = [
            'first_name'    => 'required|max:100',
            'last_name'     => 'required|max:100',
            'email'         => 'required|max:191|email',
            'password'      => 'nullable|min:8|same:confirm-password',
            'role_id'       => 'required|integer',
        ];

        if (!$id) {
            $validationRules['email'] = 'unique:users,email';
            $validationRules['password'] = 'required';
        }

        $this->validate($request, $validationRules);

        $input = $request->all();
        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        } else {
            $input = Helper::array_except($input, array('password'));
        }

        $data = null;
        $avatar = [];
        if (!$id) {
            $data = User::create($input);
            // dd($data);
        } else {
            $data = User::find($id);

            $return = \App\Helpers\Helper::notValidData($data, $this->_routePrefix . '.index');
            if ($return) {
                return $return;
            }

            $data->update($input);
        }

        if(isset($input['profile_image'])){
            $profilefile=new File();
            if(isset($data['files']['id'])){
                $profilefile=File::find($data['files']['id']);
                $filename='storage/files/'.$data['files']['name'];
                unlink(public_path($filename));
            }
            $imagename=$this->saveFile($input['profile_image']);
            $profilefile->name=$imagename['name'];
            $profilefile->file_type=$imagename['type'];
            $profilefile->original_name=$imagename['original_name'];
            $data->files()->save($profilefile);
        }
        //
        // if not owner changing their profile
        // then set role
        //
        if (isset($input['role_id']) && $input['role_id']) {
            if ($id) {
                \App\UserRole::where('user_id', $id)->delete();
            }

            \App\UserRole::create([
                'user_id' => $id ? $id : $data->id,
                'role_id' => $input['role_id']
            ]);
        }

        if (!$id) {
            return redirect()
                ->route($this->_routePrefix . '.index')
                ->with('success', 'User created succesfully');
        } else {
            return redirect()
                ->route($this->_routePrefix . '.edit', $id)
                ->with('success', 'User updated successfully');
        }
    }

    public function editProfile()
    {
        $ownAccount = true;
        $moduleName = 'Edit Profile';
        $fileLocation = '';
        $id=Auth::user()->id;
        $data = User::with('files')->find($id);

        $return = \App\Helpers\Helper::notValidData($data, $this->_routePrefix . '.index');
        if ($return) {
            return $return;
        }

        $module     = $this->_module . ' | ' . $moduleName;
        $breadcrumb = [
            route($this->_routePrefix . '.profile') => $this->_module,
            '#'                                   => $moduleName,
        ];

        $form = [
            'route'      => $this->_routePrefix . '.saveProfile',
            'back_route' => route($this->_routePrefix . '.profile'),
            'fields'     => [
                'first_name'             => [
                    'type'       => 'text',
                    'label'      => 'First Name',
                    'help'       => 'Maximum 100 characters',
                    'attributes' => ['required' => true],
                ],
                'middle_name'             => [
                    'type'       => 'text',
                    'label'      => 'Middle Name',
                    'help'       => 'Maximum 100 characters',
                ],
                'last_name'             => [
                    'type'       => 'text',
                    'label'      => 'Last Name',
                    'help'       => 'Maximum 100 characters',
                ],
                'email'            => [
                    'type'       => 'email',
                    'label'      => 'Email',
                    'help'       => 'Maximum 191 characters',
                    'attributes' => ['required' => true],
                ],
                'password'         => [
                    'type'  => 'password',
                    'label' => 'Password',
                ],
                'confirm-password' => [
                    'type'  => 'password',
                    'label' => 'Confirm Password',
                ],
                'profile_image'=>[
                    'type'=>'file',
                    'value'=>isset($data['files']) ? $data['files']['full_url']:'',
                    'attributes'=>['file_mime'=>isset($data['files']) ? $data['files']['file_type']:''],
                ]
            ],
        ];

        if ($ownAccount) {
            unset($form['back_route']);
        }

        return view('backend.components.admin-form', compact('data', 'id', 'form', 'breadcrumb', 'module'));
    }

    public function saveProfile(Request $request, $id = 0)
    {
        $validationRules = [
            'first_name'    => 'required|max:100',
            'last_name'     => 'required|max:100',
            'email'         => 'required|max:191|email',
            'password'      => 'nullable|min:8|same:confirm-password',
        ];

        //
        // if this is not own account, it will
        // require role.
        //

        $this->validate($request, $validationRules);

        $input = $request->except(['employee_id']);
        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        } else {
            $input = Helper::array_except($input, array('password'));
        }
            $data = User::with('files')->find($id);

            $return = \App\Helpers\Helper::notValidData($data, $this->_routePrefix . '.index');
            if ($return) {
                return $return;
            }
            $data->update($input);
            if(isset($input['profile_image'])){
                $profilefile=new File();
                if(isset($data['files']['id'])){
                    $profilefile=File::find($data['files']['id']);
                    $filename='storage/files/'.$data['files']['name'];
                    unlink(public_path($filename));
                }
                $imagename=$this->saveFile($input['profile_image']);
                $profilefile->name=$imagename['name'];
                $profilefile->file_type=$imagename['type'];
                $profilefile->original_name=$imagename['original_name'];
                $data->files()->save($profilefile);
            }
            return redirect()
                ->back()
                ->with('success', 'Changes has been successfully saved.');
    }

    public function togglestatus($id=''){
        try {
            $user=User::find($id);
            $user->status=$user->status?0:1;
            $user->save();
            return response()->json(['type'=>'success','message' => 'Succesfully changed status']);
        } catch (Exception $e) {
            return response()->json(['type'=>'danger','message'=>'Something went wrong']);
        }
    }
}
