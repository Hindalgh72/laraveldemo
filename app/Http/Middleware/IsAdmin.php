<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if ($user && $user->status && $user->roles()->count()) {

            // checking whether current user has permission
            // to enter the page
            $res = \App\Permission::checkPermission();
            if ($res) {
                return $next($request);
            }

        }

        Session::flash('error', 'You are not verified your account yet, or Your account is currently blocked by Administrator, or You don\'t have permission to enter this site. If you think this is wrong please contact with us.');
        return redirect('/admin/login');
    }
}
