<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Authenticatable
{
    use HasApiTokens, Notifiable,SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'status',
        'uid',
    ];

    public function getUser()
    {
        return $this->belongsTo('App\User', 'uid');
    }

    public function users()
    {
        return $this->hasMany('App\UserRole', 'role_id', 'id');
    }

    public function permissions()
    {
    	return $this->hasMany('App\PermissionRole', 'role_id', 'id');
    }
}
