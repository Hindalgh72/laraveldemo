<?php

namespace App\Listeners;

use App\Events\UploadVideoEvent;
use App\Helpers\Helper;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Vimeo\Laravel\Facades\Vimeo;

class UploadVideoListener implements ShouldQueue
{
    use Queueable,DispatchesJobs;

    
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function handle(UploadVideoEvent $event)
    {
        $lesson_id = $event->lesson_id;
        $unique_key   = $event->unique_key;
        $fileName     = $event->fileName;
        $file_id = $event->file_id;
        $file_path = $event->file_path;

        $File=\App\File::find($file_id);
        $VimeoFile=new \App\VimeoFile;
        if ($vemiourl = Vimeo::connection()->upload($file_path)) {
            $VimeoFile->vimeo_url=$vemiourl;
            $VimeoFile->file_id=$file_id;
            $File->vimeo()->save($VimeoFile);
        //Storage::disk('public')->delete('files/' . $lesson_file['name']);
        }

    }
}
