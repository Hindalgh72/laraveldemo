<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminMenu extends Model
{
    public $timestamps  = false;
    protected $fillable = [
        'parent_id',
        'display_order',
        'status',
        'class',
        'method',
        'query_string',
        'menu',
        'url',
        'icon',
    ];
    private $parent_rec = array();

    public static function getMenu($pid = 0)
    {
        $menu = self::select()
            ->where([
                'status'    => '1',
                'parent_id' => $pid,
            ])
            ->orderBy('display_order', 'ASC')
            ->get();

        if (empty($menu)) {
            return false;
        }

        $menu     = $menu->toArray();
        $userRole = \Auth::user()->roles->role_id;
        $menus    = array();
        foreach ($menu as $key => $val) {
            if ($val['class'] && $val['method']) {
                $permission = \App\Permission::checkPermission($userRole, $val['class'], $val['method']);
                if ($permission) {
                    $menus[$key]              = $val;
                    $menus[$key]['child'] = self::getMenu($val['id']);
                }
            } else {
                $menus[$key]              = $val;
                $menus[$key]['child'] = self::getMenu($val['id']);
            }
        }

        return $menus;
    }


    public function getParentMenu($id)
    {
        $p = $this->__getPreviousRecord($id);
        return $this->parent_rec;
    }

    private function __getPreviousRecord($id = 0)
    {
        $cat = self::where('id', $id)->first();

        if (!$cat) {
            $this->parent_rec = array_reverse($this->parent_rec);
            return false;
        }

        $this->parent_rec[] = $cat->toArray();
        $this->__getPreviousRecord($cat->parent_id);
    }
}
