<?php
namespace App\Helpers;
use Illuminate\Support\Arr;

use Illuminate\Support\Facades\Auth;

class Helper
{
    
    public static function array_except($array, $keys){
        return Arr::except($array, $keys);
    }
    /**
     * Show date with date format.
     * @param:      date [date / datetime]
     * @param:      show time also [boolean]
     * @version:    1.0.0.5
     * @author:     Somnath Mukherjee
     */
    public static function showdate($date, $showTime = true, $date_format = '')
    {
        if (!$date_format) {
            $date_format = 'd-M-Y';
        }
        if (self::check_valid_date($date)) {
            $gdt = explode(" ", $date);
            if ($showTime && isset($gdt[1]) && self::checktime($gdt[1])) {
                $tag = self::text_within_tag($date_format, 'tag');
                if ($tag) {
                    $textWithoutTag = str_replace("<tag>$tag</tag>", "####", $date_format);
                    $date           = (string) date($textWithoutTag, strtotime($date));
                    return str_replace("####", $tag, $date);
                } else {
                    $date = (string) date($date_format, strtotime($date));
                }

                return $date;
            } else {
                $date = (string) date($date_format, strtotime($date));
                return $date;
            }
        } else {
            return false;
        }

    }

    /**
     * Search text with html tag
     * @param:      html string [string]
     * @param:      tag name [string]
     * @return:     tag value if found else boolean
     * @version:    1.0.0.1
     * @author:     Somnath Mukherjee
     */

    public static function text_within_tag($string, $tagname)
    {
        $pattern = "#<\s*?$tagname\b[^>]*>(.*?)</$tagname\b[^>]*>#s";
        preg_match($pattern, $string, $matches);
        return isset($matches[1]) ? $matches[1] : false;
    }
    /**
     * Check valid time
     * @param:      time as string
     * @author:     Somnath Mukherjee
     * @version:    1.0.0.1
     */
    public static function checktime($time = '00:00:00')
    {
        list($hour, $min, $sec) = explode(':', $time);

        if ($hour == 0 && $min == 0 && $sec == 0) {
            return false;
        }

        if ($hour < 0 || $hour > 23 || !is_numeric($hour)) {
            return false;
        }
        if ($min < 0 || $min > 59 || !is_numeric($min)) {
            return false;
        }
        if ($sec < 0 || $sec > 59 || !is_numeric($sec)) {
            return false;
        }
        return true;
    }

    /**
     * Check valid date
     * @param:      date as string
     * @author:     Somnath Mukherjee
     * @version:    1.0.0.2
     */
    public static function check_valid_date($date = '')
    {
        if ($date != "") {
            $date = date("Y-m-d", strtotime($date));
            $dt   = explode("-", $date);
            return checkdate((int) $dt[1], (int) $dt[2], (int) $dt[0]);
        }

        return false;
    }

    /**
     * Get clean url.
     * @params:         url string
     * @version:        1.0.0.1
     * @author:         Somnath Mukherjee
     */

    public static function getcleanurl($name)
    {
        $name = trim($name);
        $url  = preg_replace('/[^A-Za-z0-9\-]/', '-', $name);
        $url  = strtolower(str_replace("-----", "-", $url));
        $url  = strtolower(str_replace("----", "-", $url));
        $url  = strtolower(str_replace("---", "-", $url));
        $url  = strtolower(str_replace("--", "-", $url));

        return rtrim($url, '-');
    }

    public static function getUniqueSlug($title = '', $table = '', $column = 'slug')
    {
        $title              = self::getcleanurl($title);
        $condition[$column] = $title;

        $nor = \DB::table($table)
            ->where($condition)
            ->count();

        if (!$nor) {
            return $title;
        }

        $title .= '-' . chr(rand(65, 90));
        return self::getUniqueSlug($title, $table, $column);
    }

    /**
     * Check for directory name
     * @param:      directory name (mandatory).
     * @author:     Somnath Mukherjee.
     * @version:    1.0.0.1
     *
     * NOTE:        It checks only into user panels assets folder
     *              if specified directory not exist then it will create it
     */
    public static function check_directory($dir_name = '')
    {
        if ($dir_name == "") {
            return false;
        }

        $filePath = public_path($dir_name);
        if (!file_exists($filePath)) {
            $oldmask = umask(0);
            mkdir($filePath, 0755);

            // copying index file
            self::cp_index($filePath);
            umask($oldmask);
        }
        return true;
    }

    /**
     * Copy index.html file to the destination folder.
     *
     * @param [string] $dest Destination folder
     */
    public static function cp_index($dest = '', $source = '')
    {
        $dest   = rtrim($dest, "/") . '/index.html';
        $source = !$source ? 'index.html' : $source;
        @copy(public_path($source), $dest);
    }

    /**
     * if this is not a valid data then it will
     * redirct to the listing page
     *
     * @param  array  $data
     * @param  string $redirectRoute redirect route
     * @param  mixed $param extra param
     * @return boolean
     */
    public static function notValidData($data = [], $redirectRoute = '', $param = [])
    {
        if (!$data) {
            return redirect()
                ->route($redirectRoute, $param)
                ->with('error', 'Not a valid data.');
        }

        return false;
    }

    public static function price($price = 0)
    {
        return '$' . number_format($price, 0);
    }

    public static function SendMail($toemail, $toName, $data, $template)
    {
        $mailtemplate = \DB::table('site_email_templates')->where(['template_type' => $template])->first();
        $Content      = $mailtemplate->template_content;

        foreach ($data as $key => $value) {
            $Content = str_replace('{{' . $key . '}}', $value, $Content);
        }

        $Content = str_replace('{{site_name}}', \Config::get('settings.company_name'), $Content);
        $Content = str_replace('{{site_url}}', url('/'), $Content);

        $subject = $mailtemplate->subject;
        $subject = str_replace('{{site_name}}', \Config::get('settings.company_name'), $subject);
        $subject = str_replace('{{site_url}}', url('/'), $subject);

        $data = ['mailcontent' => $Content];

        \Mail::send('emails.template', $data, function ($m) use ($data, $toemail, $toName, $subject) {
            $m->from(\Config::get('settings.support_mail'), \Config::get('settings.company_name'))
                ->to($toemail, $toName)
                ->subject($subject);
        });

    }

    /**
     * Make associative array from 2d array.
     *
     * @param:      2d array (mandatory)
     *
     * @param:      fields name (mandatory).  First field for associative array's Key and
     *              Second field for associative array's value.
     *
     *              NOTE: if you want to put only one field, then it will create
     *              key and value with the same name or value.
     *
     * @param:      If you want to create a simple array then pass TRUE. It will not
     *              create any associative array.
     *
     * @author:     Somnath Mukherjee.
     *
     * @version:    1.0.0.2
     */

    public static function makeSimpleArray($marray = array(), $fields = '', $make_single = false)
    {
        if ($fields == "") {
            return false;
        }

        $fields = explode(",", $fields);
        $key    = null;
        $val    = null;
        if (count($fields) > 1) {
            $key = trim($fields[0]);
            $val = trim($fields[1]);
        } else {
            $key = $val = trim($fields[0]);
        }

        $sarray = array();
        if (is_array($marray) and !empty($marray)) {
            if (!$make_single) {
                foreach ($marray as $k => $v) {
                    $sarray[$v[$key]] = trim($v[$val]);
                }
            } else {
                foreach ($marray as $k) {
                    $sarray[] = $k[$key];
                }
            }
        }

        return $sarray;
    }

    public static function getMethod()
    {
        $method        = explode("@", \Route::currentRouteAction());
        return $method = end($method);
    }

    public static function getController()
    {
        $controller        = explode("@", \Route::currentRouteAction());
        $controller        = explode('\\', $controller[0]);
        return $controller = end($controller);
    }

    public static function randomString($size = 9)
    {
        $alpha_key = '';
        $keys      = range('A', 'Z');

        for ($i = 0; $i < 2; $i++) {
            $alpha_key .= $keys[array_rand($keys)];
        }

        $length = $size - 2;

        $key  = '';
        $keys = range(0, 9);

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $alpha_key . $key;
    }

    public static function checkFolder($location = '')
    {
        if (! \File::exists($location)) {
            return \File::makeDirectory($location);
        }
        return true;
    }

    public static function getAttr($attributes = [], $return = FALSE)
    {
        $param = '';
        if(!empty($attributes)){
            foreach ($attributes as $k => $v) {
                if (!$return)
                    echo ' ' . $k . '="' . $v . '"';
                else
                    $param .= ' ' . $k . '="' . $v . '"';
            }
        }
    
        return $param;
    }
}
