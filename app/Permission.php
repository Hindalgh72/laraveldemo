<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\HasApiTokens;
use App\Helpers\Helper;

class Permission extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'p_type',
        'class',
        'method',
    ];

    public static function checkPermission($userRole = 0, $className = '', $methodName = '')
    {
        $userRole   = !$userRole ? Auth::user()->roles->role_id : $userRole;
        // if this is a super admin
        // set all permission 
        if($userRole == 1)
            return true;

        $className  = !$className ? Helper::getController() : $className;
        $methodName = !$methodName ? Helper::getMethod() : $methodName;

        $permission = self::select("permissions.*")
            ->addSelect(\DB::raw("IF(pr.permission_id, 1, 0) AS has_permission"))
            ->leftJoin("permission_roles AS pr", function ($q) use ($userRole) {
                $q->on("pr.permission_id", "=", "permissions.id")
                    ->where("pr.role_id", "=", $userRole);
            })
            ->where("permissions.class", "=", $className)
            ->where("permissions.method", "=", $methodName)
            ->first();

        // if result found
        if ($permission) {
            // if permission found
            if ($permission->has_permission) {
                return true;
            }

            return false;
        }

        // if permission not entered on parent
        // table, then it will be accessible
        // by all users.
        return true;
    }

    /*
     * Check module wise permissions. It checks given methods permissions 
     * from given class name. and return method wise permission result.
     */

    public static function checkModulePermissions($methods = ['index', 'create', 'edit', 'destroy'], $class = '') {
        $userRole = Auth::user()->roles->role_id;
        // if this is super admin, grant all access
        if($userRole == 1) {
            $permission = [];
            foreach ($methods as $value) {
                $permission[$value] = true;
            }
            return $permission;
        }

        $all_permission = TRUE;
        $roles = null;

        if (!$class) {
            $class = Helper::getController();
        }

        /*
         * get methodwise permissions
         */
        $permission = self::where('class', $class)
                ->whereIn('method', $methods)
                ->get();

        if ($permission) {
            $permission = $permission->toArray();
            $permission_ids = Helper::makeSimpleArray($permission, "id", TRUE);
            if ($permission_ids) {
                $roles = \App\PermissionRole::where('role_id', $userRole)
                        ->whereIn('permission_id', $permission_ids)
                        ->get();

                if ($roles->count()) {
                    $roles = $roles->toArray();
                    $roles = Helper::makeSimpleArray($roles, "permission_id", TRUE);
                    $module_permission = array();
                    foreach ($permission as $key => $val) {
                        $module_permission[$val['method']] = in_array($val['id'], $roles);
                    }
                    $permission = $module_permission;
                } else {
                    $all_permission = FALSE;
                }
            }
        }

        if (count($methods) > count($permission)) {
            foreach ($methods as $val) {
                if (!isset($permission[$val])) {
                    $permission[$val] = $all_permission;
                }
            }
        }


        if (!$permission || ! $roles) {
            $permission = array();
            foreach ($methods as $val) {
                $permission[$val] = $all_permission;
            }
        }

        return $permission;
    }
}
