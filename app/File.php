<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;


class File extends Model
{
    protected $table = 'files';

    protected $appends=array('FullUrl');
    protected $fillable = [
        'name',
        'file_type',
        'original_name',
    ];

    public function table()
    {
        return $this->morphTo();
    }

        /**
     * Get the user's full name.
     *
     * @return string
     */
        public function getFullUrlAttribute()
        {
            return env('APP_URL').'/laraveldemo/public/storage/files/'.$this->name;
        }
    
}
