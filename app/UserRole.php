<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
	public $timestamps = false;

	protected $fillable = [
        'user_id', 
        'role_id', 
    ];
	
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
