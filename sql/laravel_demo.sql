-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 30, 2020 at 10:08 PM
-- Server version: 8.0.19-0ubuntu5
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_menus`
--

CREATE TABLE `admin_menus` (
  `id` int UNSIGNED NOT NULL,
  `parent_id` int UNSIGNED DEFAULT '0',
  `display_order` smallint UNSIGNED DEFAULT '0',
  `status` tinyint UNSIGNED DEFAULT '1',
  `class` varchar(50) DEFAULT NULL,
  `method` varchar(50) DEFAULT NULL,
  `query_string` varchar(255) DEFAULT NULL,
  `menu` varchar(100) DEFAULT NULL,
  `url` varchar(250) DEFAULT '#',
  `icon` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Admin menu';

--
-- Dumping data for table `admin_menus`
--

INSERT INTO `admin_menus` (`id`, `parent_id`, `display_order`, `status`, `class`, `method`, `query_string`, `menu`, `url`, `icon`) VALUES
(3, 2, 0, 1, 'PermissionController', 'index', NULL, 'Permissions', 'permissions.index', 'nav-icon icon-star'),
(4, 2, 0, 1, 'RoleController', 'showRoles', NULL, 'Roles', 'roles.index', 'nav-icon icon-star'),
(12, 0, 0, 1, 'PermissionController', 'index', NULL, 'Permissions', 'permissions.index', 'ion-locked'),
(13, 0, 0, 1, 'RoleController', 'index', NULL, 'Roles', 'roles.index', 'ion-ios-people'),
(15, 0, 0, 1, 'UserController', 'index', NULL, 'Users', 'users.index', 'ion-person');

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE `class` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(200) NOT NULL,
  `code` varchar(50) NOT NULL,
  `user_id` int UNSIGNED NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int UNSIGNED NOT NULL,
  `table_id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `original_name` varchar(255) DEFAULT NULL COMMENT 'Original file name',
  `file_type` varchar(255) NOT NULL,
  `table_type` varchar(255) NOT NULL,
  `status` tinyint NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `table_id`, `name`, `original_name`, `file_type`, `table_type`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '15881819361264dd.jpg', 'dd.jpg', 'image/jpeg', 'App\\User', 1, '2020-04-29 10:57:54', '2020-04-29 12:08:56');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int UNSIGNED NOT NULL,
  `p_type` varchar(255) NOT NULL,
  `class` varchar(50) NOT NULL,
  `method` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `p_type`, `class`, `method`, `created_at`, `updated_at`) VALUES
(1, 'Role List', 'RoleController', 'index', '2020-03-21 03:28:04', '2020-03-21 03:28:04'),
(2, 'Role Create', 'RoleController', 'create', '2020-03-21 01:53:28', '2020-03-21 01:53:28'),
(3, 'Role Edit', 'RoleController', 'edit', '2020-03-21 01:54:01', '2020-03-21 01:54:01'),
(4, 'Role Delete', 'RoleController', 'destroy', '2020-03-21 01:54:34', '2020-03-21 01:54:34'),
(5, 'Permission List', 'PermissionController', 'index', '2020-03-21 02:03:16', '2020-03-21 02:03:34'),
(6, 'Permission Create', 'PermissionController', 'create', '2020-03-21 02:03:55', '2020-03-21 02:03:55'),
(7, 'Permission edit', 'PermissionController', 'edit', '2020-03-21 02:04:12', '2020-03-21 02:04:12'),
(8, 'Permission Delete', 'PermissionController', 'destroy', '2020-03-21 02:05:04', '2020-03-21 02:05:04'),
(9, 'Role Management', 'PermissionController', 'manageRole', '2020-03-21 02:05:04', '2020-03-21 02:05:04'),
(10, 'User Create', 'UserController', 'create', '2020-03-21 03:13:14', '2020-04-26 04:51:37'),
(11, 'User List', 'UserController', 'index', '2020-03-21 03:28:26', '2020-03-21 03:28:26'),
(12, 'User Edit', 'UserController', 'edit', '2020-03-21 03:47:24', '2020-03-21 03:47:24'),
(13, 'User Delete', 'UserController', 'destroy', '2020-03-21 03:47:24', '2020-03-21 03:47:24'),
(14, 'permission list list', 'PermissionController', 'index', '2020-04-28 22:41:40', '2020-04-28 22:41:40'),
(15, 'permission list add', 'PermissionController', 'create', '2020-04-28 22:41:40', '2020-04-28 22:41:40'),
(16, 'permission list edit', 'PermissionController', 'edit', '2020-04-28 22:41:40', '2020-04-28 22:41:40'),
(17, 'permission list delete', 'PermissionController', 'destroy', '2020-04-28 22:41:40', '2020-04-28 22:41:40'),
(18, 'role list list', 'RoleController', 'index', '2020-04-29 10:13:25', '2020-04-29 10:13:25'),
(19, 'role list add', 'RoleController', 'create', '2020-04-29 10:13:25', '2020-04-29 10:13:25'),
(20, 'role list edit', 'RoleController', 'edit', '2020-04-29 10:13:25', '2020-04-29 10:13:25'),
(21, 'role list delete', 'RoleController', 'destroy', '2020-04-29 10:13:25', '2020-04-29 10:13:25'),
(22, 'role list list', 'RoleController', 'index', '2020-04-29 10:14:55', '2020-04-29 10:14:55'),
(23, 'role list add', 'RoleController', 'create', '2020-04-29 10:14:55', '2020-04-29 10:14:55'),
(24, 'role list edit', 'RoleController', 'edit', '2020-04-29 10:14:56', '2020-04-29 10:14:56'),
(25, 'role list delete', 'RoleController', 'destroy', '2020-04-29 10:14:56', '2020-04-29 10:14:56');

-- --------------------------------------------------------

--
-- Table structure for table `permission_roles`
--

CREATE TABLE `permission_roles` (
  `permission_id` int UNSIGNED NOT NULL,
  `role_id` int UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permission_roles`
--

INSERT INTO `permission_roles` (`permission_id`, `role_id`) VALUES
(9, 2),
(2, 2),
(3, 2),
(1, 2),
(10, 2),
(12, 2),
(11, 2),
(14, 1),
(15, 1),
(16, 1),
(17, 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int UNSIGNED NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `uid` int UNSIGNED NOT NULL,
  `is_private` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `title`, `uid`, `is_private`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Developer', 1, 1, 1, '2020-03-20 02:08:09', '2020-03-30 00:32:36', NULL),
(2, 'Admin', 1, 1, 1, '2020-03-30 00:32:15', '2020-03-30 00:32:30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE `site_settings` (
  `id` int UNSIGNED NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_logo` text NOT NULL,
  `company_favicon` text NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int UNSIGNED NOT NULL,
  `first_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `middle_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` bigint DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint UNSIGNED NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `middle_name`, `last_name`, `email`, `phone`, `password`, `remember_token`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Hindal', NULL, 'Ghosh', 'hindal@yopmail.com', NULL, '$2y$10$cemTyjOz8Z43ggqkiixoXOlPFcNk0KOLX.NqUfWM8.wPEc.owPKcW', NULL, 1, '2020-03-20 02:42:30', '2020-04-28 10:28:07', NULL),
(2, 'Super', NULL, 'Admin', 'superadmin@yopmail.com', 1236547895, '$2y$10$JSVYiWXHSW2FW64bxjUsAOlHOBrr1CekUakkpht3iIxIa9A96cdNS', NULL, 1, '2020-04-26 10:13:31', '2020-04-28 09:20:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` int UNSIGNED NOT NULL,
  `role_id` int UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`user_id`, `role_id`) VALUES
(1, 1),
(2, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_menus`
--
ALTER TABLE `admin_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_roles`
--
ALTER TABLE `permission_roles`
  ADD KEY `pid` (`permission_id`),
  ADD KEY `rid` (`role_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD KEY `role_id` (`role_id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_menus`
--
ALTER TABLE `admin_menus`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `class`
--
ALTER TABLE `class`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `class`
--
ALTER TABLE `class`
  ADD CONSTRAINT `class_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_roles`
--
ALTER TABLE `permission_roles`
  ADD CONSTRAINT `permission_roles_ibfk_1` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
