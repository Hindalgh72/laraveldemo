$(".filter-btn").click(function(){
    $(".filter-frm-wrap").toggle();
});

function confirmDelete(delId) {
    Swal.fire({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this data!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "Not sure"
    }).then(function (result) {
        if (result.value) {
            $("#" + delId).submit();
        } else {
            Swal.fire({
                title: "Your data is safe!",
                timer: 1500
            });
        }
    });
}

$(window).on("load", function () {
    $(".delete-btn").click(function (e) {
        var delId = $(this).data("form-id");
        e.preventDefault();
        confirmDelete(delId);
    });
});