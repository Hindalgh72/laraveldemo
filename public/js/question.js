
var templete = '<div class="input-group form-group"><div class="input-group-prepend"><span class="input-group-text"><input type="checkbox" name="is_correct[]" value="${optionText}" class="is_correct"></span></div><input type="text" value="${optionText}" class="form-control option" name="option[]"><div class="input-group-append"><div class="input-group-text"><i class="fas fa-trash" onClick="removeOption(this,\'${id}\',\'${counter}\')"></i></div></div></div><div class="custom-file custom-file-question form-group" id="imageopt_${counter}"><input type="file" class="custom-file-input custom-file-input-validation" name="optionfile[]" id="customFile${counter}"><label class="custom-file-label" for="customFile${counter}">Choose file</label></div>';
var templetetruefalse = '<div class="input-group form-group"><div class="input-group-prepend"><span class="input-group-text"><input type="radio" name="is_correct[]" value="${optionText}" class="is_correct"><input type="hidden" value="${optionText}" name="option[]" class="option"></span></div><div class="form-control">${optionText}</div></div>';

var optiontemplete = '<input type="hidden" value="${id}" name="optionids[]" id="hideo_${id}">';

var optionimagetemplete = '<div class="img-wrap" id="imgo_${counter}"><img src="${fullurl}" width="200"><a href="javascript:void(0)" class="remove_f" onClick="removeQuestionFile("${id}",this)"><i class="fas fa-trash"></i></a><div>';

var optionLimit=4;0

$(document).ready(function () {
  
  $('#option').change(function (e) {
    let option_value = $(this).val();
    $(this).prev().find('.is_correct').val() = option_value;
  });


  $('#add_option').click(function (e) {
    $('#option-error').html('');
    let optionText = $('#option_text').val();
    console.log(optionText);
    let currentOptionCount = $('.option').length
    if (currentOptionCount >= optionLimit){
      // Swal("You have addedd maximun options.", "", "error");
      Swal.fire({
        icon: 'error',
        text: 'You have addedd maximun options.',
      })
      return false;
    }
    if (optionText!=''){
      // $('#mcq_option_div').append(templete);
      $.tmpl(templete, { optionText: optionText, id: null, counter: currentOptionCount }).appendTo('#option_div');
      $.tmpl('<hr>', {}).appendTo('#option_div');
      $('#option_text').val('');
      bsCustomFileInput.init();
    }
  });

  // jQuery.validator.addMethod("checkoption", function (value, element) {
    
  //   let optionChecked=$('.is_correct:checked').length
  //   if (value == 'multiple_choice' && (optionChecked == 0 || typeof $(".is_correct").val()=='undefined')){
  //     return false;
  //   }
  //   else if (value == 'true_false' && optionChecked == 0){
  //     return false;
  //   }
  //   return true;
  // }, ""); 


  jQuery.validator.addMethod("checkQuestionImage", function (value, element) {
    console.log(value, element);
    var file = document.getElementById("question_file").files[0];
    if (typeof file!='undefined'){
      var t = file.type.split('/').pop().toLowerCase();
      if (t != "jpeg" && t != "jpg" && t != "png") {
        return false;
      }
    }
    
    return true;
  }, ""); 

  jQuery.validator.addMethod("checkOptionImage", function (value, element) {
    console.log('checkOptionImage add ');
    if (value == ''){
      return true;
    }
    var t = value.split('.').pop().toLowerCase();
    console.log(t);
    if (t != "jpeg" && t != "jpg" && t != "png") {
      return false;
    }
    return true;
  }, "Please select valid option image"); 

  jQuery.validator.addClassRules("custom-file-input-validation", {
    checkOptionImage: true
  });

  $("#questionForm").validate({
    rules: {
      type: { required: true }, 
      description: { required: true },
      InputQuestion: "required",
      question_file: { checkQuestionImage:true}
    },
    messages: {
      InputQuestion: "Please enter question", 
      desctiption: "Please enter question Description",
      question_type:{required: "Please select question type"},
      question_file: { checkQuestionImage: "Please select valid question image" }
    },
    submitHandler: function (form) {
      $('#option-error').html('');
      let value=$('#type').val();
      let optionChecked = $('.is_correct:checked').length;
      if (value == 'multiple_choice' && (optionChecked == 0 || typeof $(".is_correct").val() == 'undefined')) {
        setOptionErrorMessage('Please add and select right options.');
        return false;
      }
      else if (value == 'true_false' && optionChecked == 0) {
        setOptionErrorMessage('Please select right options.');
        return false;
      }
      return true;
      //$(form).submit();
    }
  });


});
function setOptionErrorMessage(message) {
  $('#option-error').html(''); 
  $('#option-error').show();
  $('#option-error').html(message); 
}
function enableFunctionForMCQ(){
  $('#option_div').html('');
  console.log('ewfwefw')
  $('#mcq_option_add_wrapper').show();
}

function initFunctionForChange() {
  $('#mcq_option_add_wrapper').hide();
  $('#option_div').html('');
}

function enableFunctionForTrueFalse() {
  $('#option_div').html('');
  let values=['true','false']
  for (let i = 0; i <=1; i++) {
    $.tmpl(templetetruefalse, { optionText: values[i] }).appendTo('#option_div');
    $('#option_text').val('');
  }
}

